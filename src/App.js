import { useEffect, useReducer, useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Container } from "react-bootstrap";
import AppNavBar from "./components/AppNavbar";
import { UserProvider } from "./Usercontext";
import { CartProvider } from "./CartContext";
import { LoadingModalProvider } from "./LoadingModalContext";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Logout from "./pages/Logout";
import LoadingModal from "./components/LoadingModal";
import AdminDashboard from "./pages/AdminDashBoard";
import AdminProducts from "./pages/Admin/AdminProducts";
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import Checkout from "./pages/Checkout";
import Orders from "./pages/Orders";
import AdminOrders from "./pages/Admin/AdminOrders";
import AdminUsers from "./pages/AdminUsers";
import "./styles.css";

export default function App() {
  const [user, userDispatch] = useReducer(userReducer, {
    id: null,
    isAdmin: null,
    cartItems: [],
    totalAmountPayable: 0,
  });
  const [showCart, showCartDispatch] = useReducer(showCartReducer, false);
  const [showLoadingModal, modalDispatch] = useReducer(modalReducer, false);
  useEffect(() => {
    if (!localStorage.getItem("token")) {
      return;
    }
    fetch(`${process.env.REACT_APP_API_URL}/users/user/profile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          console.log("app useEffect data", data);
          userDispatch({
            type: "set_user",
            user: data,
          });
        }
      });
  }, []);
  console.log("app after useEffect data", user);

  return (
    <UserProvider value={{ user, userDispatch }}>
      <CartProvider value={{ showCart, showCartDispatch }}>
        <LoadingModalProvider value={{ showLoadingModal, modalDispatch }}>
          <Router>
            <Container className="mx-0 px-0" fluid>
              <AppNavBar className="mx-0" />
              <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/login" element={<Login />} />
                <Route path="/register" element={<Register />} />
                <Route path="/logout" element={<Logout />} />
                <Route path="/products" element={<Products />} />
                <Route path="/checkout" element={<Checkout />} />
                <Route path="/orders" element={<Orders />} />
                <Route path="/products/:productId" element={<ProductView />} />
                <Route path="/adminDashboard" element={<AdminDashboard />} />
                <Route path="/admin/products" element={<AdminProducts />} />
                <Route path="/admin/orders" element={<AdminOrders />} />
                <Route path="/admin/users" element={<AdminUsers />} />
              </Routes>
              {/* <LoadingModal /> */}
            </Container>
          </Router>
        </LoadingModalProvider>
      </CartProvider>
    </UserProvider>
  );
}

function userReducer(state, action) {
  console.log("userReducer action", action);
  switch (action.type) {
    case "set_user": {
      return {
        id: action.user._id,
        isAdmin: action.user.isAdmin,
        cartItems: [...action.user.cartItems],
        totalAmountPayable: action.user.totalAmountPayable ?? 0,
      };
    }
    case "unset_user": {
      localStorage.clear();
      return {
        id: null,
        isAdmin: null,
        cartItems: [],
        totalAmountPayable: 0,
      };
    }
    case "set_user_cart": {
      return {
        ...state,
        cartItems: [...action.cartItems],
        totalAmountPayable: action.totalAmountPayable,
      };
    }
  }
}

function showCartReducer(state, action) {
  console.log("showCartReducer action", action);
  switch (action.type) {
    case "show_cart": {
      return true;
    }
    case "hide_cart": {
      return false;
    }
  }
}

function modalReducer(state, action) {
  switch (action.type) {
    case "show": {
      return true;
    }
    case "hide": {
      return false;
    }
  }
}
