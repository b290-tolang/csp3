import { useContext, useEffect, useState } from "react";
import { Container, Row, Col, Card, Accordion, Button } from "react-bootstrap";
import { NavLink, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";
import UserContext from "../Usercontext";

export default function Orders() {
  const { user } = useContext(UserContext);
  const [orders, setOrders] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    // Navigate to Login page if user is not logged in
    if (!localStorage.getItem("token")) {
      console.log("navigating to login");
      navigate("/login");
      return;
    }

    fetch(`${process.env.REACT_APP_API_URL}/users/user/orders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log("orders useeffect data", data);
        setOrders([...data.reverse()]);
      });
  }, []);

  // Navigate non logged in user to Login page

  return (
    <Container fluid style={{ backgroundColor: "#f5f5f5", minHeight: "100vh" }}>
      <Row>
        <Col md={{ span: 6, offset: 3 }}>
          <h1>Orders</h1>
          <Accordion alwaysOpen defaultActiveKey={0}>
            {orders.map((order, i) => (
              <Accordion.Item key={order._id} eventKey={i}>
                <Accordion.Header>
                  Order #: {order._id}{" "}
                  <span className="ms-4 fw-bold">{order.orderStatus}</span>
                </Accordion.Header>
                <Accordion.Body>
                  <div>
                    <Container fluid className="p-3">
                      <Row>
                        <Col md={5}>
                          <span className="fw-bold">Product Ordered</span>
                        </Col>
                        <Col md={1} className="text-center">
                          <span>Qty</span>
                        </Col>
                        <Col md={2} className="text-end">
                          <span>Price</span>
                        </Col>
                        <Col md={4} className="text-end"></Col>
                      </Row>
                    </Container>
                  </div>
                  {order.products.map((item) => (
                    <ItemEntry
                      key={item._id}
                      item={item}
                      productId={item.productId}
                    />
                  ))}
                  <div>
                    <Container fluid className="p-3">
                      <Row>
                        <Col>
                          <div className="text-start">
                            <span className="fw-bold">Order Total</span>
                            <span className="fs-4">
                              : ${order.totalAmount.toFixed(2)}
                            </span>
                          </div>
                        </Col>
                      </Row>
                    </Container>
                  </div>
                </Accordion.Body>
              </Accordion.Item>
            ))}
          </Accordion>
        </Col>
      </Row>
    </Container>
  );
}

function ItemEntry({ productId, item }) {
  const [product, setProduct] = useState({});

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProduct(data);
      });
  }, []);

  return (
    <div>
      <Container fluid className="">
        <Row>
          <Col md={5}>
            <Link to={`/products/${productId}`}>
              <span>{product.name}</span>
            </Link>
          </Col>
          <Col md={1} className="text-center">
            <span>{item.quantity}</span>
          </Col>
          <Col md={2} className="text-end">
            <span>${item.price}</span>
          </Col>
          <Col md={4}>
            <Link>Rate this Product!</Link>{" "}
          </Col>
        </Row>
      </Container>
    </div>
  );
}
