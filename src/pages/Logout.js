import { useContext, useEffect } from "react";
import UserContext from "../Usercontext";
import { Navigate } from "react-router-dom";

export default function Logout() {
    const {user, userDispatch}  = useContext(UserContext)

    localStorage.clear()
    
    useEffect(() => {
        console.log('logout useEffect')
        userDispatch({type: 'unset_user'})
    }, [])
    
    console.log('logout after useEffect', user)
    return (
        <Navigate to='/login' />
    )
}