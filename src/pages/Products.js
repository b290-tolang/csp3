import { useEffect, useReducer, useContext } from "react";
import { Container, Row, Col } from "react-bootstrap";
import ProductCard from "../components/ProductCard";
import LoadingModalContext from "../LoadingModalContext";

export default function Products() {
  const [products, dispatch] = useReducer(productsReducer, []);
  const { modalDispatch } = useContext(LoadingModalContext);

  useEffect(() => {
    modalDispatch({ type: "show" });
    fetch(`${process.env.REACT_APP_API_URL}/products/`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log("fetch data", data);
        dispatch({ type: "fill_products", products: data });
        modalDispatch({ type: "hide" });
      });
  }, []);
  console.log("products page", products);
  return (
    <Container>
      <Row>
        <Col>
          <div>
            <h1 className="mt-3">Active Products</h1>
            <div className="content d-flex">
              <div
                id="side"
                className="p-3"
                style={{ width: "200px", backgroundColor: "lightcyan" }}
              >
                <h3>Categories</h3>
              </div>
              <div id="body-content">
                <div id="products-container" className="d-flex flex-wrap p-3">
                  {products.map((product) => (
                    <ProductCard
                      key={product._id}
                      product={product}
                      dispatch={dispatch}
                    />
                  ))}
                </div>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

function productsReducer(state, action) {
  switch (action.type) {
    case "fill_products": {
      return [...action.products];
    }
  }
}
