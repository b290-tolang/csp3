import { useContext, useEffect, useState } from "react";
import UserContext from "../Usercontext";
import CartContext from "../CartContext";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

export default function Checkout() {
  const { user, userDispatch } = useContext(UserContext);
  const { showCartDispatch } = useContext(CartContext);
  const [isActive, setIsActive] = useState(true);

  const navigate = useNavigate();

  function handleIsActive(bool) {
    setIsActive(() => bool);
  }

  return (
    <Container fluid style={{ backgroundColor: "#f5f5f5", minHeight: "100vh" }}>
      <Row>
        <Col md={{ span: 4, offset: 4 }}>
          <div className="mt-5">
            <Container fluid className="p-3">
              <Row>
                <Col md={7}>
                  <span className="fw-bold">Product Ordered</span>
                </Col>
                <Col md={1} className="text-center">
                  <span>Qty</span>
                </Col>
                <Col md={4} className="text-end">
                  <span>Price</span>
                </Col>
              </Row>
            </Container>
          </div>
          <div className="">
            {user.cartItems.map((item) => (
              <CheckoutEntry key={item.productId} item={item} />
            ))}
          </div>
          <div id="summary"></div>
          <div className="mt-3">
            <Card>
              <Card.Header>
                <span className="fw-bold">Summary</span>
              </Card.Header>
              <Card.Body>
                <div className="d-flex">
                  <Card.Title className="text-center">Total</Card.Title>
                  <Card.Text className="ms-auto">
                    <span className="fs-4 fw-bold">
                      ${user.totalAmountPayable.toFixed(2)}
                    </span>
                  </Card.Text>
                </div>
                <div className="text-end mt-4">
                  <Button
                    onClick={() => {
                      console.log("Order placed");
                      placeOrder()
                        .then((user) => {
                          userDispatch({ type: "set_user", user: user });
                          //   console.log("pre navigate", user);
                          Swal.fire({
                            icon: "success",
                            title: "Order has been placed",
                            text: `Happy Shopping!`,
                          });
                          navigate("/orders");
                          //   console.log("after navigate", user);
                        })
                        .catch((err) => {
                          Swal.fire({
                            icon: "error",
                            title: "Something went wrong",
                            text: err.message,
                          });
                        });
                    }}
                    className="px-5 py-2"
                    style={{ backgroundColor: "#ffa33a", border: "none" }}
                  >
                    Place Order
                  </Button>
                </div>
              </Card.Body>
            </Card>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

function CheckoutEntry({ item }) {
  const [product, setProduct] = useState({});

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${item.productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProduct(data);
      });
  }, []);

  return (
    <Card>
      <Container fluid className="p-3">
        <Row>
          <Col md={7}>
            <h6 className="fw-bold">{product.name}</h6>
            <p>Stocks available: {product.stockQuantity}</p>
          </Col>
          <Col md={2} className="text-center">
            <span>{item.quantity}</span>
          </Col>
          <Col md={3} className="text-end  fs-4">
            $<span>{item.price}</span>
          </Col>
        </Row>
      </Container>
    </Card>
  );
}

async function placeOrder() {
  const data = await fetch(
    `${process.env.REACT_APP_API_URL}/users/user/cart/checkout`,
    {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    }
  )
    .then(async (res) => {
      try {
        return await res.json();
      } catch (err) {
        throw new Error("Insufficient product/s in this order.");
      }
    })
    .then((data) => {
      console.log("checkout fetch data", data);
      if (data) {
        return fetch(`${process.env.REACT_APP_API_URL}/users/user/profile`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });
      }
    })
    .then((res) => res.json());

  console.log("fetch profile after place order", data);
  return data;
}
