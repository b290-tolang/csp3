import { useEffect, useState, useReducer, useContext } from "react";
import { Button, Container, Modal } from "react-bootstrap";
import { Form } from "react-bootstrap";
import ProductTable from "../../components/ProductTable";
import LoadingModalContext from "../../LoadingModalContext";

export default function AdminProducts() {
  const [products, dispatch] = useReducer(productsReducer, []);
  const [showImagesModal, imagesModalDispatch] = useReducer(
    imagesModalReducer,
    { show: false, product: {} }
  );
  const [addProductMode, setAddProductMode] = useState(false);
  const { modalDispatch } = useContext(LoadingModalContext);
  useEffect(() => {
    modalDispatch({ type: "show" });
    fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        dispatch({ type: "fill_products", products: data });
        modalDispatch({ type: "hide" });
      });
  }, []);

  const handleAddProduct = () => setAddProductMode(!addProductMode);

  console.log("images modal", showImagesModal);
  return (
    <Container className="text-center">
      <h1 className="mt-3">All Products</h1>
      <Button onClick={handleAddProduct}>Add Product</Button>
      <ProductTable
        className="mt-3"
        addProductMode={addProductMode}
        products={products}
        dispatch={dispatch}
        imagesModalDispatch={imagesModalDispatch}
      />

      {/* MODAL FOR ADDING IMAGES */}
      <Modal
        centered
        show={showImagesModal.show}
        onHide={() => imagesModalDispatch({ type: "hide" })}
        backdrop="static"
        keyboard={false}
        style={{ zIndex: 5000 }}
      >
        <Modal.Header closeButton>
          <Modal.Title>{showImagesModal.product.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Label>Add Image URLs</Form.Label>
          <Form.Group>
            <Form.Label>#1</Form.Label>
            <Form.Control
              value={
                showImagesModal.product.images &&
                showImagesModal.product.images[0]
              }
              onChange={(e) => {
                imagesModalDispatch({
                  type: "set_images1",
                  image1: e.target.value,
                });
              }}
              type="text"
              aria-describedby="passwordHelpBlock"
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>#2</Form.Label>
            <Form.Control
              value={
                showImagesModal.product.images &&
                showImagesModal.product.images[1]
              }
              onChange={(e) => {
                imagesModalDispatch({
                  type: "set_images2",
                  image2: e.target.value,
                });
              }}
              type="text"
              aria-describedby="passwordHelpBlock"
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>#3</Form.Label>
            <Form.Control
              value={
                showImagesModal.product.images &&
                showImagesModal.product.images[2]
              }
              onChange={(e) => {
                imagesModalDispatch({
                  type: "set_images3",
                  image3: e.target.value,
                });
              }}
              type="text"
              aria-describedby="passwordHelpBlock"
            />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => imagesModalDispatch({ type: "hide" })}
          >
            Close
          </Button>
          {/* Click event will save the image urls in the modal,
          to the product */}
          <Button
            variant="primary"
            onClick={() => {
              addImagesFetch(showImagesModal.product).then((data) => {
                console.log("add images", data);
                imagesModalDispatch({ type: "hide" });
              });
            }}
          >
            Save Image Urls
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}

async function addImagesFetch(product) {
  console.log("add image product", product);
  const data = await fetch(
    `${process.env.REACT_APP_API_URL}/products/${product._id}`,
    {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        images: [...product.images],
      }),
    }
  ).then((res) => res.json());
  return data;
}

function productsReducer(products, action) {
  switch (action.type) {
    case "fill_products":
      return [...action.products];
    case "edit_product": {
      return products.map((product) => {
        if (product._id === action._id) {
          return action.product;
        }
        return product;
      });
    }
    case "sort": {
      switch (action.sortBy) {
        case "name":
        case "category": {
          return [...products].sort((a, b) => {
            if (a[action.sortBy] < b[action.sortBy]) {
              console.log("lt");
              return 1;
            }
            if (a[action.sortBy] > b[action.sortBy]) {
              console.log("gt");
              return -1;
            }
            return 0;
          });
        }
        case "price":
        case "stockQuantity":
        case "soldQuantity": {
          return [...products].sort(
            (a, b) => a[action.sortBy] - b[action.sortBy]
          );
        }
      }
    }
  }
}

function imagesModalReducer(state, action) {
  switch (action.type) {
    case "show": {
      return {
        ...state,
        show: true,
        product: { ...action.product, images: [...action.product.images] },
      };
    }
    case "hide": {
      return { ...state, show: false };
    }
    case "set_images1": {
      return {
        ...state,
        product: {
          ...state.product,
          images: [...state.product.images, action.image1],
        },
      };
    }
    case "set_images2": {
      return {
        ...state,
        product: {
          ...state.product,
          images: [...state.product.images, action.image2],
        },
      };
    }
    case "set_images3": {
      return {
        ...state,
        product: {
          ...state.product,
          images: [...state.product.images, action.image3],
        },
      };
    }
  }
}
