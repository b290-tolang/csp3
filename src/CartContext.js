import { createContext } from "react";

const CartContext = createContext(false)

export const CartProvider = CartContext.Provider
export default CartContext