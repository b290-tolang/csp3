import { useState } from "react";
import { Button } from "react-bootstrap";

export default function AddProduct({ products, dispatch }) {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");
  const [price, setPrice] = useState("");
  const [stockQuantity, setStockQuantity] = useState("");
  const [soldQuantity, setSoldQuantity] = useState("");

  const [isLoading, setIsLoading] = useState(false);

  if (isLoading) {
    return (
      <tr>
        <td colSpan={6}>
          <div className="d-flex justify-content-center">
            <div className="spinner-border" role="status">
              <h1 className="visually-hidden">Loading...</h1>
            </div>
          </div>
        </td>
      </tr>
    );
  }

  return (
    <tr>
      <td>
        <input
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
        ></input>
      </td>
      <td>
        <textarea
          rows={3}
          cols={30}
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        ></textarea>
      </td>
      <td>
        <input
          size={10}
          type="text"
          value={category}
          onChange={(e) => setCategory(e.target.value)}
        ></input>
      </td>
      <td>
        <input
          size={5}
          type="text"
          value={price}
          onChange={(e) => setPrice(e.target.value)}
        ></input>
      </td>
      <td>
        <input
          size={5}
          type="text"
          value={stockQuantity}
          onChange={(e) => setStockQuantity(e.target.value)}
        ></input>
      </td>
      <td>
        <input
          size={5}
          type="text"
          value={soldQuantity}
          onChange={(e) => setSoldQuantity(e.target.value)}
        ></input>
      </td>
      <td>
        <Button
          style={{ width: 80 }}
          className="btn-sm"
          variant="primary"
          onClick={() => {
            setIsLoading(true);
            const product = {
              name,
              description,
              category,
              price,
              stockQuantity,
              soldQuantity,
            };
            addProductFetch({
              name,
              description,
              category,
              price,
              stockQuantity,
              soldQuantity,
            }).then((data) => {
              dispatch({ type: "fill_products", products: data });
              setIsLoading(false);
              setName("");
              setDescription("");
              setCategory("");
              setPrice("");
              setStockQuantity("");
              setSoldQuantity("");
            });
          }}
        >
          Add
        </Button>
      </td>
      <td></td>
    </tr>
  );
}

// This function is used to make fetch request to add a product, and also to get
// the updated list of all products
async function addProductFetch(product) {
  const data = await fetch(`${process.env.REACT_APP_API_URL}/products/`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify(product),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data) {
        return fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });
      }
    })
    .then((res) => res.json());
  console.log("addProductFetch data", data);

  return data;
}
