import { useContext, useState } from "react";
import { Container, Row, Col, Button, Modal, Form, Nav } from "react-bootstrap";
import Table from "react-bootstrap/Table";
import { NavLink } from "react-router-dom";
import LoadingModalContext from "../LoadingModalContext";
import AddProduct from "./AddProduct";

export default function ProductTable({
  products,
  dispatch,
  addProductMode,
  imagesModalDispatch,
}) {
  console.log("productTable", products);
  return (
    <Container>
      <Row>
        <Col>
          <Table className="text-start">
            <thead>
              <tr>
                <th>
                  <NavLink
                    onClick={() => dispatch({ type: "sort", sortBy: "name" })}
                  >
                    Product Name
                  </NavLink>
                </th>
                <th>Description</th>
                <th>
                  <NavLink
                    onClick={() =>
                      dispatch({ type: "sort", sortBy: "category" })
                    }
                  >
                    Category
                  </NavLink>
                </th>
                <th>
                  <NavLink
                    onClick={() => dispatch({ type: "sort", sortBy: "price" })}
                  >
                    Price
                  </NavLink>
                </th>
                <th>
                  <NavLink
                    onClick={() =>
                      dispatch({ type: "sort", sortBy: "stockQuantity" })
                    }
                  >
                    Stock
                  </NavLink>
                </th>
                <th>
                  <NavLink
                    onClick={() =>
                      dispatch({ type: "sort", sortBy: "soldQuantity" })
                    }
                  >
                    Sold
                  </NavLink>
                </th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {addProductMode ? (
                <AddProduct products={products} dispatch={dispatch} />
              ) : null}
              {products
                .map((product) => (
                  <ProductEntry
                    dispatch={dispatch}
                    key={product._id}
                    product={product}
                    imagesModalDispatch={imagesModalDispatch}
                  />
                ))
                .reverse()}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
}

function ProductEntry({ product, dispatch, imagesModalDispatch }) {
  const [editMode, setEditMode] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const { modalDispatch } = useContext(LoadingModalContext);

  // duplicate product properties will be used during Editing of product,
  //  so that product will only be edited if Save button is pressed
  const [name, setName] = useState(product.name);
  const [description, setDescription] = useState(product.description);
  const [category, setCategory] = useState(product.category);
  const [price, setPrice] = useState(product.price);
  const [stockQuantity, setStockQuantity] = useState(product.stockQuantity);
  const [soldQuantity, setSoldQuantity] = useState(product.soldQuantity);

  if (isLoading) {
    return (
      <tr>
        <td colSpan={8}>
          <div className="d-flex justify-content-center">
            <div className="spinner-border" role="status">
              <h1 className="visually-hidden">Loading...</h1>
            </div>
          </div>
        </td>
      </tr>
    );
  }

  return (
    <>
      {editMode ? (
        <tr>
          <td>
            <input
              value={name}
              onChange={(e) => setName(e.target.value)}
            ></input>
          </td>
          <td>
            <textarea
              rows={3}
              cols={30}
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            ></textarea>
          </td>
          <td>
            <input
              size={10}
              value={category}
              onChange={(e) => setCategory(e.target.value)}
            ></input>
          </td>
          <td>
            <input
              size={5}
              value={price}
              onChange={(e) => setPrice(e.target.value)}
            ></input>
          </td>
          <td>
            <input
              size={5}
              value={stockQuantity}
              onChange={(e) => setStockQuantity(e.target.value)}
            ></input>
          </td>
          <td>
            <input
              size={5}
              value={soldQuantity}
              onChange={(e) => setSoldQuantity(e.target.value)}
            ></input>
          </td>
          <td>
            <Button
              style={{ width: 80 }}
              className="btn-sm"
              variant="success"
              onClick={() => {
                setIsLoading(true);
                // update products in memory
                dispatch({
                  type: "edit_product",
                  _id: product._id,
                  product: {
                    ...product,
                    name,
                    description,
                    category,
                    price,
                    stockQuantity,
                    soldQuantity,
                  },
                });
                // update products in database
                updateFetch({
                  ...product,
                  name,
                  description,
                  category,
                  price,
                  stockQuantity,
                  soldQuantity,
                }).then((data) => {
                  setIsLoading(false);
                  setEditMode(false);
                });
              }}
            >
              Save
            </Button>
          </td>
          <td>
            {/* BUTTON TO SHOW IMAGES MODAL  */}
            <Button
              onClick={() => {
                imagesModalDispatch({
                  type: "show",
                  show: true,
                  product: product,
                });
              }}
              style={{ width: 80 }}
              className="btn-sm"
            >
              Images
            </Button>
          </td>
        </tr>
      ) : (
        <tr>
          <td>{product.name}</td>
          <td>{product.description}</td>
          <td>{product.category}</td>
          <td>${product.price}</td>
          <td>{product.stockQuantity}</td>
          <td>{product.soldQuantity}</td>
          <td>
            <Button
              disabled={!product.isActive}
              style={{ width: 80 }}
              className="btn-sm"
              variant="success"
              onClick={() => setEditMode(true)}
            >
              Edit
            </Button>
          </td>
          <td>
            <Button
              style={{ width: 80 }}
              className="btn-sm"
              onClick={() => {
                product.isActive
                  ? dispatch({
                      type: "edit_product",
                      _id: product._id,
                      product: { ...product, isActive: false },
                    })
                  : dispatch({
                      type: "edit_product",
                      _id: product._id,
                      product: { ...product, isActive: true },
                    });

                setIsLoading(true);
                updateFetch({ ...product, isActive: !product.isActive }).then(
                  (data) => {
                    setIsLoading(false);
                  }
                );
              }}
              variant={product.isActive ? "danger" : "info"}
            >
              {product.isActive ? "Archive" : "Activate"}
            </Button>
          </td>
        </tr>
      )}
    </>
  );
}

async function updateFetch(product) {
  console.log("updateFetch product", product);
  const {
    isActive,
    name,
    category,
    description,
    price,
    stockQuantity,
    soldQuantity,
  } = product;

  const data = await fetch(
    `${process.env.REACT_APP_API_URL}/products/${product._id}`,
    {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        isActive,
        name,
        category,
        description,
        price,
        stockQuantity,
        soldQuantity,
      }),
    }
  ).then((res) => res.json());

  console.log("updateFetch data", data);

  return data;
}
